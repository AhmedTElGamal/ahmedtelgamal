//package eg.edu.alexu.csd.filestructure.graphs;
//
//import java.io.File;
//import java.util.ArrayList;
//
//public class Main {
//	public static void main(String args[]) {
//		MyGraph myGraph = new MyGraph();
//		File file = new File("example.txt");
//		final int INF = 999999999;
//		int[] distances = new int[1000000];
//		int v;
//		myGraph.readGraph(file);
//		ArrayList<Integer> verticies = myGraph.getVertices();
//		for(Integer int1 : verticies){
//			System.out.print(int1 + " ");
//		}
//		v = 10;
//		for(int i=0 ; i<v ; i++){
//			distances[i] = INF;
//		}
//		myGraph.runDijkstra(0, distances);
//	}
//}